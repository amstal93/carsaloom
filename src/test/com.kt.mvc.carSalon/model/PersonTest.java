package com.kt.mvc.carSalon.model;


import com.kt.mvc.carSalon.controller.carCreatorController.MenuInterface;
import com.kt.mvc.carSalon.controller.printerController.PrinterController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

public class PersonTest {

    Person person;

    @BeforeEach
    public void setup(){
        person = new Person();
    }

    @Test
    public void shouldHaveMoneyWhenMoneyAdded(){
        //when
        person.addMoney(10000);
        //then
        assertEquals(10000,person.getCurrentWallet());
    }

    @Test
    public void shouldHaveLessMoneyWhenMoneySubstracted(){
        //when
        person.addMoney(10000);
        person.subtractMoney(5000);
        //then
        assertEquals(5000,person.getCurrentWallet());
    }
}