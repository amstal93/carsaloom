package com.kt.mvc.carSalon.model;

import com.kt.mvc.carSalon.controller.carCreatorController.MenuInterface;
import com.kt.mvc.carSalon.controller.printerController.PrinterController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;


public class BrandTest {

    Person person;
    Person personPoor;
    Car car;
    MenuInterface menuInterface;
    PrinterController printerController;

    @BeforeEach
    public void setup(){
        person = new Person();
        personPoor = new Person();
        personPoor.addMoney(0);
        person.addMoney(1000000);

        car = new Car();
        car.setBrand(Brand.NONE);
        car.setBrand(Brand.NONE);
        car.setColour(Colour.NONE);
        car.setFuelType(FuelType.NONE);
        car.setUpholstery(Upholstery.NONE);

        menuInterface = mock(MenuInterface.class);
        printerController = mock(PrinterController.class);
    }

    @ParameterizedTest
    @EnumSource(value = Brand.class, names = {"VOLVO", "AUDI", "LEXUS", "CHRYSLER", "BENTLEY"})
    public void shouldNotAllowToSelectOptionWhenPersonHaveNotEnoughtMoney(Brand brandType) {
        //when
        brandType.checkPickedBrand(personPoor, car, menuInterface, printerController);
        //then
        assertAll(
                () -> verify(menuInterface).goToBrandPick(),
                () -> verify(printerController).println(anyString())
        );
    }

    @ParameterizedTest
    @EnumSource(value = Brand.class, names = {"VOLVO", "AUDI", "LEXUS", "CHRYSLER", "BENTLEY"})
    public void shouldAllowToSelectOptionWhenPersonHaveEnoughtMoney(Brand brandType) {
        //when
        brandType.checkPickedBrand(person, car, menuInterface, printerController);
        //then
        assertAll(
                () -> assertEquals(person.getCurrentWallet(), 1000000 - brandType.getPrice()),
                () -> assertEquals(car.getBrand(), brandType),
                () -> verifyNoMoreInteractions(printerController),
                () -> verifyNoMoreInteractions(menuInterface)
        );
    }
}