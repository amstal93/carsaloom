package com.kt.mvc.carSalon.model;

import com.kt.mvc.carSalon.controller.carCreatorController.MenuInterface;
import com.kt.mvc.carSalon.controller.printerController.PrinterController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class ColourTest {

    Person person;
    Person personPoor;
    Car car;
    MenuInterface menuInterface;
    PrinterController printerController;

    @BeforeEach
    public void setup(){
        person = new Person();
        personPoor = new Person();
        personPoor.addMoney(0);
        person.addMoney(1000000);

        car = new Car();
        car.setBrand(Brand.NONE);
        car.setBrand(Brand.NONE);
        car.setColour(Colour.NONE);
        car.setFuelType(FuelType.NONE);
        car.setUpholstery(Upholstery.NONE);

        menuInterface = mock(MenuInterface.class);
        printerController = mock(PrinterController.class);
    }


    @ParameterizedTest
    @EnumSource(value = Colour.class, names = {"GOLD", "RED"})
    public void shouldNotAllowToSelectOptionWhenPersonHaveNotEnoughtMoney(Colour colour) {
        //when
        colour.checkPickedColour(personPoor, car, menuInterface, printerController);
        //then
        assertAll(
                () -> verify(menuInterface).goToColourPick(),
                () -> verify(printerController).println(anyString())
        );
    }

    @ParameterizedTest
    @EnumSource(value = Colour.class, names = {"GOLD", "RED"})
    public void shouldAllowToSelectOptionWhenPersonHaveEnoughtMoney(Colour colour) {
        //when
        colour.checkPickedColour(person, car, menuInterface, printerController);
        //then
        assertAll(
                () -> assertEquals(person.getCurrentWallet(), 1000000 - colour.getPrice()),
                () -> assertEquals(car.getColour(), colour),
                () -> verifyNoMoreInteractions(printerController),
                () -> verifyNoMoreInteractions(menuInterface)
        );
    }

    @ParameterizedTest
    @EnumSource(value = Colour.class, names = {"WHITE", "BLACK", "SILVER"})
    public void shouldAllowToSelectFreeOptionWhenPersonSelectIt(Colour colour) {
        //when
        colour.checkPickedColour(personPoor, car, menuInterface, printerController);
        colour.checkPickedColour(person, car, menuInterface, printerController);
        //then
        assertAll(
                () -> assertEquals(person.getCurrentWallet(), 1000000 - colour.getPrice()),
                () -> assertEquals(car.getColour(), colour),
                () -> verifyNoMoreInteractions(printerController),
                () -> verifyNoMoreInteractions(menuInterface)
        );
    }
}