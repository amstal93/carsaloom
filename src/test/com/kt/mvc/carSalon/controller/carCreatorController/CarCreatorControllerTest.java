package com.kt.mvc.carSalon.controller.carCreatorController;

import com.kt.mvc.carSalon.controller.printerController.PrinterMVC;
import com.kt.mvc.carSalon.model.*;
import org.junit.jupiter.api.BeforeEach;
import com.kt.mvc.carSalon.controller.scannerController.ScannerMVC;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class CarCreatorControllerTest {

    Person person;
    Car car;
    PrinterMVC.Controller printer;
    ScannerMVC.Controller scanner;
    CarCreatorController tested;

    @BeforeEach
    public void setup() {
        person = new Person();
        person.addMoney(1000000);

        car = new Car();
        car.setBodyType(BodyType.NONE);
        car.setBrand(Brand.NONE);
        car.setColour(Colour.NONE);
        car.setFuelType(FuelType.NONE);
        car.setUpholstery(Upholstery.NONE);

        printer = mock(PrinterMVC.Controller.class);
        scanner = mock(ScannerMVC.Controller.class);
        tested = new CarCreatorController(scanner, car, person, printer);
    }

    @ParameterizedTest
    @MethodSource("provideColours")
    public void shouldSetColourWhenColourSelected(int option, Colour colour) {
        //given
        when(scanner.pickOption()).thenReturn(option);
        //when
        tested.pickColor();
        //then
        assertEquals(car.getColour(), colour);
    }

    private static Stream<Arguments> provideColours() {
        return Stream.of(
                Arguments.of(1, Colour.WHITE),
                Arguments.of(2, Colour.BLACK),
                Arguments.of(3, Colour.SILVER),
                Arguments.of(4, Colour.GOLD),
                Arguments.of(5, Colour.RED)
        );
    }

    @ParameterizedTest
    @MethodSource("provideFuelType")
    public void shouldSetFuleTypeWhenFuelTypeSelected(int option, FuelType fuelType) {
        //given
        when(scanner.pickOption()).thenReturn(option);
        //when
        tested.pickFuelType();
        //then
        assertEquals(car.getFuelType(), fuelType);
    }

    private static Stream<Arguments> provideFuelType() {
        return Stream.of(
                Arguments.of(1, FuelType.BENZYNA),
                Arguments.of(2, FuelType.DIESEL),
                Arguments.of(3, FuelType.HYBRYDA)
        );
    }

    @ParameterizedTest
    @MethodSource("provideBrandTypes")
    public void shouldSetBrandWhenBrandSelected(int option, Brand brand) {
        //given
        when(scanner.pickOption()).thenReturn(option);
        //when
        tested.pickBrand();
        //then
        assertEquals(car.getBrand(), brand);
    }

    private static Stream<Arguments> provideBrandTypes() {
        return Stream.of(
                Arguments.of(1, Brand.VOLVO),
                Arguments.of(2, Brand.AUDI),
                Arguments.of(3, Brand.LEXUS),
                Arguments.of(4, Brand.CHRYSLER),
                Arguments.of(5, Brand.BENTLEY)
        );
    }

    @ParameterizedTest
    @MethodSource("provideBodyTypes")
    public void shouldSetBodyTypeWhenBodyTypeSelected(int option, BodyType bodyType) {
        //given
        when(scanner.pickOption()).thenReturn(option);
        //when
        tested.pickBodyType();
        //then
        assertEquals(car.getBodyType(), bodyType);
    }

    private static Stream<Arguments> provideBodyTypes() {
        return Stream.of(
                Arguments.of(1, BodyType.SEDAN),
                Arguments.of(2, BodyType.PICKUP),
                Arguments.of(3, BodyType.HATCHBACK),
                Arguments.of(4, BodyType.KOMBI)
        );
    }

    @ParameterizedTest
    @MethodSource("provideUpholstery")
    public void shouldSetUpholsteryWhenUpholsterySelected(int option, Upholstery upholstery) {
        //given
        when(scanner.pickOption()).thenReturn(option);
        //when
        tested.pickUpholstery();
        //then
        assertEquals(car.getUpholstery(), upholstery);
    }

    private static Stream<Arguments> provideUpholstery() {
        return Stream.of(
                Arguments.of(1, Upholstery.WELUR),
                Arguments.of(2, Upholstery.SKORA),
                Arguments.of(3, Upholstery.SKORA_PIKOWANA)
        );
    }
}