package com.kt.mvc.carSalon.controller.carCreatorController;

import com.kt.mvc.carSalon.model.Car;

public interface CarCreatorControllerMVC {
    interface Controller {
        void pickColor();
        void pickFuelType();
        void pickBodyType();
        void pickBrand();
        void pickUpholstery();
        Car returnCar();
    }
}
