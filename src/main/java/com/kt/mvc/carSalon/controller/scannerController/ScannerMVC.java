package com.kt.mvc.carSalon.controller.scannerController;

public interface ScannerMVC {
    interface Controller {

        int nextInt();

        int pickOption();
    }
}
