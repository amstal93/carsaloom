package com.kt.mvc.carSalon.model;

import com.kt.mvc.carSalon.controller.carCreatorController.MenuInterface;
import com.kt.mvc.carSalon.controller.printerController.PrinterMVC;

public enum Colour {
    WHITE(0),
    NONE(0),
    BLACK(0),
    SILVER(0),
    GOLD(1000),
    RED(1000);
    
    private int price;

    Colour(int price) {
        this.price = price;
    }
    public int getPrice() {
        return price;
    }

    public void checkPickedColour(Person person, Car car, MenuInterface menuInterface, PrinterMVC.Controller printer) {
        if (person.getCurrentWallet() + car.getColour().getPrice() >= this.getPrice()) {
            person.addMoney(car.getColour().getPrice());
            car.setColour(this);
            person.subtractMoney(this.getPrice());

        } else {
            printer.println("Not enough money!");
            menuInterface.goToColourPick();
        }
    }
}