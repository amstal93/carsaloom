package com.kt.mvc.carSalon.model;

import com.kt.mvc.carSalon.controller.printerController.PrinterController;
import com.kt.mvc.carSalon.controller.carCreatorController.MenuInterface;
import com.kt.mvc.carSalon.controller.printerController.PrinterMVC;

public enum Upholstery {

    WELUR(0),
    NONE (0),
    SKORA(1000),
    SKORA_PIKOWANA(2000);

    private int price;

    Upholstery(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void checkPickedUpholstery(Person person, Car car, MenuInterface menuInterface, PrinterMVC.Controller printer) {
        if (person.getCurrentWallet() + car.getUpholstery().getPrice() >= this.getPrice()) {
            person.addMoney(car.getUpholstery().getPrice());
            car.setUpholstery(this);
            person.subtractMoney(this.getPrice());
        } else {
            printer.println("Not enough money!");
            menuInterface.goToUpholsteryPick();
        }
    }
}
